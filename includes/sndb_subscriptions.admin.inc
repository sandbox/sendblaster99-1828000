<?php



/**
 * Store filter values in session var
 *
 * @param $type identification string
 * @param $form_values array of values to be stored
 */
function _sndb_subscriptions_set_filter($type, $values) {
  if (empty($_SESSION[$type])) {
    $_SESSION[$type] = 'all';
  }
  $op = $values['op'];
  if ($op == t('Filter') && isset($values['filter'])) {
    $_SESSION[$type] = $values['filter'];
  }
}


/**
 * Menu callback: Mass subscribe to newsletters.
 *
 * @see sndb_subscriptions_subscription_list_add_submit()
 * @todo Add 32char description field as subscription source
 */
function sndb_subscriptions_subscription_list_add() {
  global $language;

  $form['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Email addresses'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Email addresses must be separated by comma, space or newline.'),
  );
  

  // Include language selection when the site is multilingual.
  // Default value is the empty string which will result in receiving emails
  // in the site's default language.
  if (variable_get('language_count', 1) > 1) {
    $options[''] = t('Site default language');
    $languages = language_list('enabled');
    foreach ($languages[1] as $langcode => $item) {
      $name = t($item->name);
      $options[$langcode] = $name . ($item->native != $name ? ' ('. $item->native .')' : '');
    }
    $form['language'] = array(
      '#type' => 'radios',
      '#title' => t('Anonymous user preferred language'),
      '#default_value' => '',
      '#options' => $options,
      '#description' => t('New anonymous mail addresses will be subscribed with the selected preferred language. Registered users will be subscribed with their preferred language as set on their account page. The language of existing subscribers is unchanged.'),
    );
  }
  else {
    $form['language'] = array('#type' => 'value', '#value' => '');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );
  return $form;
}

function sndb_subscriptions_subscription_list_add_submit($form, &$form_state) {
  $added = array();
  $invalid = array();
  //$checked_newsletters = array_filter($form_state['values']['newsletters']);
  $langcode = $form_state['values']['language'];

  $emails = preg_split("/[\s,]+/", $form_state['values']['emails']);
  foreach ($emails as $email) {
    $email = trim($email);
    if ($email == '') {
      continue;
    }
    if (sndb_subscriptions_valid_email_address($email)) {
      sndb_subscriptions_subscribe_user($email, $newsletter->tid, FALSE, 'mass subscribe', $langcode);
      $added[] = $email;
    }
    else {
      $invalid[] = $email;
    }
  }
  if ($added) {
    $added = implode(", ", $added);
    drupal_set_message(t('The following addresses were added or updated: %added.', array('%added' => $added)));
  }
  else {
    drupal_set_message(t('No addresses were added.'));
  }
  if ($invalid) {
    $invalid = implode(", ", $invalid);
    drupal_set_message(t('The following addresses were invalid: %invalid.', array('%invalid' => $invalid)), 'error');
  }
}

/**
 * Menu callback: Export email address of subscriptions.
 *
 * @see sndb_subscriptions_admin_export_after_build()
 * @todo Add checkboxes for subscribed and unsubscribed selection.
 */
function sndb_subscriptions_subscription_list_export() {
  $form['states'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Status'),
    '#options' => array('active' => t('Active users'), 'inactive' => t('Inactive users')),
    '#description' => t('Subscriptions matching the selected states will be exported.'),
    '#required' => TRUE,
  );
  
  /*
  $newsletters = array();
  foreach (sndb_subscriptions_get_newsletters(variable_get('sndb_subscriptions_vid', ''), TRUE) as $newsletter) {
    $newsletters[$newsletter->tid] = check_plain($newsletter->name);
  }
  $form['newsletters'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Subscribed to'),
    '#options' => $newsletters,
    '#description' => t('Subscriptions matching the selected newsletters will be exported.'),
    '#required' => TRUE,
  );*/

  // Emails item is initially empty. It serves as place holder. Data is added by
  // sndb_subscriptions_admin_export_after_build().
  $form['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Export results'),
    '#cols' => 60,
    '#rows' => 5,
    '#value' => '',
    '#access' => FALSE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
  );
  $form['#after_build'] = array('sndb_subscriptions_admin_export_after_build');
  $form['#redirect'] = FALSE;
  return $form;
}

function sndb_subscriptions_admin_export_after_build($form, $form_element) {
  if (isset($form_element['values']['op']) && $form_element['values']['op'] == t('Export')) {
    $states = array_filter($form_element['values']['states']);
    //$newsletters = array_filter($form_element['values']['newsletters']);

    // Build where clause for active/inactive state and newsletter selection.
    if (isset($states['active'])) {
      $where[] = 's.activated = 1';
    }
    if (isset($states['inactive'])) {
      $where[] = 's.activated = 0';
    }
    $where = isset($where) ? implode(' OR ', $where) : NULL;
    
    // Get subscription data
    if (isset($where)) {
      $query = '
        SELECT DISTINCT s.mail
        FROM {sndb_subscriptions} s
        WHERE (' . $where . ')';
      $result = db_query($query);
      while ($mail = db_fetch_object($result)) {
        $mails[] = $mail->mail;
      }
    }

    // Build form field containing exported emails.
    // The field will be included in the form where at the ['emails'] place holder.
    if (isset($mails)) {
      $exported_mails = implode(", ", $mails);
    }
    else {
      $exported_mails = t('No addresses were found.');
    }
    $form['emails']['#value'] = $exported_mails;
    $form['emails']['#access'] = TRUE;
  }
  return $form;
}

/**
 * Menu callback: Mass subscribe to newsletters.
 *
 * @see sndb_subscriptions_subscription_list_remove_submit()
 * @todo Add 32char description field as unsubscription source
 */
function sndb_subscriptions_subscription_list_remove() {
  $form['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Email addresses'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Email addresses must be separated by comma, space or newline.'),
  );
  
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Unsubscribe'),
  );
  return $form;
}

function sndb_subscriptions_subscription_list_remove_submit($form, &$form_state) {
  $removed = array();
  $invalid = array();

  $emails = preg_split("/[\s,]+/", $form_state['values']['emails']);
  foreach ($emails as $email) {
    $email = trim($email);
    if (sndb_subscriptions_valid_email_address($email)) {
      sndb_subscriptions_unsubscribe_user($email, 0, FALSE, 'mass unsubscribe');
      $removed[] = $email;
    }
    else {
      $invalid[] = $email;
    }
  }
  if ($removed) {
    $removed = implode(", ", $removed);
    drupal_set_message(t('The following addresses were unsubscribed: %removed.', array('%removed' => $removed)));
  }
  else {
    drupal_set_message(t('No addresses were removed.'));
  }
  if ($invalid) {
    $invalid = implode(", ", $invalid);
    drupal_set_message(t('The following addresses were invalid: %invalid.', array('%invalid' => $invalid)), 'error');
  }
}

/**
 * Menu callback: subscription administration.
 **/
function sndb_subscriptions_subscription_admin($form_state) {
  /* SNDB: deletion confirmation is skipped
  // Delete subscriptions requires delete confirmation. This is handled with a different form
  if (isset($form_state['post']['operation']) && $form_state['post']['operation'] == 'delete' && isset($form_state['post']['snids'])) {
    $destination = drupal_get_destination();
    $_SESSION['sndb_subscriptions_subscriptions_delete'] = $form_state['post']['snids'];
    // Note: we redirect from admin/content/simplenews/users to admin/content/simplenews/subscriptions/delete to make the tabs disappear.
    drupal_goto("admin/content/simplenews/subscriptions/delete", $destination);
  }*/

  $form = sndb_subscriptions_subscription_filter_form();
  $form['admin'] = sndb_subscriptions_subscription_list_form();

  return $form;
}

/**
 * Build the form for admin subscription.
 *
 * Form consists of a filter fieldset, an operation fieldset and a list of
 * subscriptions matching the filter criteria.
 *
 * @see sndb_subscriptions_subscription_list_form_validate()
 * @see sndb_subscriptions_subscription_list_form_submit()
 */
function sndb_subscriptions_subscription_list_form() {
//TODO: Subscriber maintenance needs overhaul now we have more data available.

  // Table header. Used as tablesort default
  $header = array(
    array('data' => t('Email'), 'field' => 'ss.mail', 'sort' => 'asc'),
    array('data' => t('Username'), 'field' => 'u.name'),
    array('data' => t('Status'), 'field' => 'ss.activated'),
  );
  if (variable_get('language_count', 1) > 1) {
    $header[] = array('data' => t('Language'), 'field' => 'ss.language');
  }
  $header[] = t('Operations');

  // Data collection with filter and sorting applied
  $filter = sndb_subscriptions_build_subscription_filter_query();
  $query = '
    SELECT DISTINCT ss.snid, ss.*, u.name
    FROM {sndb_subscriptions} ss
    LEFT JOIN {users} u
      ON ss.uid = u.uid
    ' . $filter['where'];
  // $count_query used to count distinct records only, match per line
  $count_query = preg_replace('/SELECT.*/', 'SELECT COUNT(DISTINCT ss.snid)', $query);
  $query .= tablesort_sql($header);
  $result = pager_query($query, 30, 0, $count_query);

  // Update options
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => array(
      'activate' => t('Activate'),
      'inactivate' => t('Inactivate'),
      'delete' => t('Delete'),
    ),
    '#default_value' => 'activate');
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('sndb_subscriptions_subscription_list_form_submit'),
    '#validate' => array('sndb_subscriptions_subscription_list_form_validate'),
  );

  $snids = array();
  // Subscription table and table pager
  $languages = language_list();
  while ($subscription = db_fetch_object($result)) {
    $snids[$subscription->snid] = '';
    $form['mail'][$subscription->snid] = array('#value' => $subscription->mail);
    $form['name'][$subscription->snid] =  array('#value' => isset($subscription->uid) ? l($subscription->name, 'user/'. $subscription->uid) : $subscription->name);
    $form['status'][$subscription->snid] = array('#value' => theme('sndb_subscriptions_status', $subscription->activated, 'activated'));
    if (variable_get('language_count', 1) > 1) {
      $form['language'][$subscription->snid] = array('#value' => $languages[$subscription->language]->name);
    }
    //$form['operations'][$subscription->snid] = array('#value' => l(t('edit'), 'admin/settings/sndb_subscriptions/users/edit/'. $subscription->snid, array(), drupal_get_destination()));
  }
  $form['snids'] = array('#type' => 'checkboxes', '#options' => $snids);
  $form['pager'] = array('#value' => theme('pager', NULL, 30, 0));
  $form['#theme'] = 'sndb_subscriptions_subscription_list';
  return $form;
}

function sndb_subscriptions_subscription_list_form_validate($form, &$form_state) {
  if (isset($form_state['values']['operation'])) {
    $snids = array_filter($form_state['values']['snids']);
    if (empty($snids)) {
      form_set_error('', t('No items selected.'));
    }
  }
}

function sndb_subscriptions_subscription_list_form_submit($form, &$form_state) {
  if (isset($form_state['values']['operation'])) {
    $snids = array_filter($form_state['values']['snids']);
    $args = array($snids);
    switch ($form_state['values']['operation']) {
      case 'activate':
        call_user_func_array('sndb_subscriptions_activate_subscription', $args);
        drupal_set_message(t('The update has been performed.'));
        break;
      case 'inactivate':
        call_user_func_array('sndb_subscriptions_inactivate_subscription', $args);
        drupal_set_message(t('The update has been performed.'));
        break;
      case 'delete':
        call_user_func_array('sndb_subscriptions_delete_multiple_subscriptions', $args);
        drupal_set_message(t('Email subscriptions were deleted.'));
        break;
    }
  }
}


/**
 * Menu callback: SNDB admin settings.
 */
function sndb_subscriptions_admin_settings_subscription(&$form_state) {
  $vid = variable_get('sndb_subscriptions_vid', '');
  $address_default = variable_get('site_mail', ini_get('sendmail_from'));
  $form = array();

  $form['sndb_subscriptions_subscription']['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('User account'),
    '#collapsible' => FALSE,
  );
  $form['sndb_subscriptions_subscription']['account']['sndb_subscriptions_sync_account'] = array(
    '#type' => 'checkbox',
    '#title' => t('Synchronize with account'),
    '#default_value' => variable_get('sndb_subscriptions_sync_account', TRUE),
    '#description' => t('When checked subscriptions will be synchronized with site accounts. When accounts are deleted, subscriptions with the same email address will be removed. When site accounts are blocked/unblocked, subscriptions will be deactivated/activated. When not checked subscriptions will be unchanged when associated accounts are deleted or blocked.'),
  );
  
  
  
  // check for token, if it doens't exist, create it.
  if(($token = variable_get('sndb_subscriptions_export_link_token', FALSE)) == FALSE) {
    $token = drupal_get_token();
    variable_set('sndb_subscriptions_export_link_token', $token);
  }
  
  $active_export_link_path = url('sndb_subscriptions/export/active/' . $token,
    array (
      'absolute' => TRUE,
    )
  );
  
  $inactive_export_link_path = url('sndb_subscriptions/export/inactive/' . $token,
    array (
      'absolute' => TRUE,
    )
  );
  $form['sndb_subscriptions_subscription']['export_link'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export links'),
    '#collapsible' => FALSE,
    '#description' => t('When enabled, all subscriptions will be available remotely in CSV format.<br />
      For active susbcriptions use: !link_active.<br />
      For inactive susbcriptions use: !link_inactive.',
      array('!link_active' => l($active_export_link_path, $active_export_link_path),
             '!link_inactive' => l($inactive_export_link_path, $inactive_export_link_path))
    ),
  );
  
  $form['sndb_subscriptions_subscription']['export_link']['sndb_subscriptions_export_link_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Export link enabled'),
    '#default_value' => variable_get('sndb_subscriptions_export_link_enabled', FALSE),
  );

  
  
  $form['sndb_subscriptions_subscription']['block_descriptions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block descriptions'),
    '#collapsible' => TRUE,
    '#description' => t('What text should be displayed in the bock.')
  );
  $form['sndb_subscriptions_subscription']['block_descriptions']['sndb_subscriptions_block_desc_above_fields'] = array(
    '#type' => 'textarea',
    '#title' => t('Text to display above subscription fields'),
    '#default_value' => variable_get('sndb_subscriptions_block_desc_above_fields', t('Here you can subscribe to our newsletter.')),
    '#rows' => 3,
  );
  $form['sndb_subscriptions_subscription']['block_descriptions']['sndb_subscriptions_block_desc_below_fields'] = array(
    '#type' => 'textarea',
    '#title' => t('Text to display below subscription fields and buttons'),
    '#default_value' => variable_get('sndb_subscriptions_block_desc_below_fields', ''),
    '#rows' => 3,
  );
  
  

  $form['sndb_subscriptions_subscription']['extra_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional fields'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('You can specify extra fields, that will be available in the subscription form. If the field caption is not entered, it is ignored.')
  );
  
  for($i = 1; $i <= SNDB_SUBSCRIPTION_EXTRA_FIELDS_MAX_CNT; $i++) {
    $form['sndb_subscriptions_subscription']['extra_fields']["sndb_subscriptions_extra_field_{$i}_title"] = array(
      '#type' => 'textfield',
      '#title' => t("Field $i title"),
      '#default_value' => variable_get("sndb_subscriptions_extra_field_{$i}_title", ''),
    );
  }
  
  
  
  // These email tokens are shared for all settings, so just define
  // the list once to help ensure they stay in sync.

  $form['sndb_subscriptions_subscription']['subscription_mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Confirmation emails'),
    '#description' => t('Confirmation email texts for double opt-in signup.'),
    '#collapsible' => TRUE,
  );

  $form['sndb_subscriptions_subscription']['subscription_mail']['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['sndb_subscriptions_subscription']['subscription_mail']['token_help']['help'] = array(
    '#value' => theme('token_help', 'sndb_subscriptions'),
  );

  $form['sndb_subscriptions_subscription']['subscription_mail']['sndb_subscriptions_confirm_subscribe_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => _sndb_subscriptions_subscription_confirmation_text('subscribe_subject'),
    '#maxlength' => 180,
  );
  $form['sndb_subscriptions_subscription']['subscription_mail']['sndb_subscriptions_confirm_subscribe_unsubscribed'] = array(
    '#type' => 'textarea',
    '#title' => t('Body text of subscribe email'),
    '#default_value' => _sndb_subscriptions_subscription_confirmation_text('subscribe_unsubscribed'),
    '#rows' => 5,
  );
  $form['sndb_subscriptions_subscription']['subscription_mail']['sndb_subscriptions_confirm_subscribe_subscribed'] = array(
    '#type' => 'textarea',
    '#title' => t('Body text for already subscribed visitor'),
    '#default_value' => _sndb_subscriptions_subscription_confirmation_text('subscribe_subscribed'),
    '#rows' => 5,
  );
  $form['sndb_subscriptions_subscription']['subscription_mail']['sndb_subscriptions_confirm_unsubscribe_subscribed'] = array(
    '#type' => 'textarea',
    '#title' => t('Body text of unsubscribe email'),
    '#default_value' => _sndb_subscriptions_subscription_confirmation_text('unsubscribe_subscribed'),
    '#rows' => 5,
  );
  $form['sndb_subscriptions_subscription']['subscription_mail']['sndb_subscriptions_confirm_unsubscribe_unsubscribed'] = array(
    '#type' => 'textarea',
    '#title' => t('Body text for not yet subscribed visitor'),
    '#default_value' => _sndb_subscriptions_subscription_confirmation_text('unsubscribe_unsubscribed'),
    '#rows' => 5,
  );

  /*
  $form['sndb_subscriptions_subscription']['confirm_pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Confirmation pages'),
    '#description' => t('If defined, these pages show after subscription / removal instead of the regular message box.'),
    '#collapsible' => FALSE,
  );
  $form['sndb_subscriptions_subscription']['confirm_pages']['sndb_subscriptions_confirm_subscribe_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Subscribe confirmation'),
    '#description' => t('Drupal path or URL of the destination page where after the subscription is confirmed (e.g. node/123). Leave empty to go to the front page.'),
    '#default_value' => variable_get('sndb_subscriptions_confirm_subscribe_page', ''),
  );
  $form['sndb_subscriptions_subscription']['confirm_pages']['sndb_subscriptions_confirm_unsubscribe_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Unsubscribe confirmation'),
    '#description' => t('Drupal path or URL of the destination page when the subscription removal is confirmed (e.g. node/123). Leave empty to go to the front page.'),
    '#default_value' => variable_get('sndb_subscriptions_confirm_unsubscribe_page', ''),
  );*/

  return system_settings_form($form);
}

/**
 * Callback function to activate the specified subscriptions.
 *
 * @param $snid array of snid's
 */
function sndb_subscriptions_activate_subscription($snid) {
  db_query('
    UPDATE {sndb_subscriptions}
    SET activated = %d
    WHERE snid IN('. db_placeholders($snid, 'int') .')',
    array_merge(
      array(SNDB_SUBSCRIPTION_STATUS_SUBSCRIBED),
      $snid));
}

/**
 * Callback function to inactivate the specified subscriptions.
 *
 * @param $snid array of snid's
 */
function sndb_subscriptions_inactivate_subscription($snid) {
  db_query('
    UPDATE {sndb_subscriptions}
    SET activated = %d
    WHERE snid IN('. db_placeholders($snid, 'int') .')',
    array_merge(
      array(SNDB_SUBSCRIPTION_STATUS_UNSUBSCRIBED),
      $snid));
}

/**
 * Callback function to delete the specified subscriptions.
 *
 * @param $snid array of snid's
 */
function sndb_subscriptions_delete_multiple_subscriptions($snid) {
  db_query('
      DELETE FROM {sndb_subscriptions}
      WHERE snid IN('. db_placeholders($snid, 'int') .')',
        $snid);
}

/**
 * Built filter selection box options and filter query where clause
 *
 * @param $type identification string
 * @param $na TRUE for orphaned newsletters
 *
 * @return array of filter selection box options and related query where clause
 */
function sndb_subscriptions_get_filter($type, $na = TRUE) {
  //Default data
  $names['all'] = t('all newsletters');
  $queries['all'] = '';
  if ($na) {
    $names['na'] = t('orphaned newsletters');
    $queries['na'] = 's.tid = 0';
  }
  // Data for each newsletter
  foreach (sndb_subscriptions_get_newsletters(variable_get('sndb_subscriptions_vid', ''), TRUE) as $newsletter) {
    $names[$newsletter->tid] = $newsletter->name;
    $queries[$newsletter->tid] = 's.tid = '. $newsletter->tid;
  }
  return array($names, $queries);
}

/**
 * Generate subscription filters
 */
function sndb_subscriptions_subscription_filters() {

  // Email filter
  $filters['email'] = array(
    'title' => t('Email address'),
  );

  return $filters;
}

/**
 * Return form for subscription filters.
 *
 * @see sndb_subscriptions_subscription_filter_form_submit()
 */
function sndb_subscriptions_subscription_filter_form() {
  // Current filter selections in $session var; stored at form submission
  // Example: array('newsletter' => 'all', 'email' => 'hotmail')
  $session = isset($_SESSION['sndb_subscriptions_subscriptions_filter']) ? $_SESSION['sndb_subscriptions_subscriptions_filter'] : '';
  $session = is_array($session) ? $session : _sndb_subscriptions_subscription_filter_default();
  $filters = sndb_subscriptions_subscription_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription filters'),
    '#collapsible' => FALSE,
    '#prefix' => '<div class="sndb-subscriptions-subscription-filter">',
    '#suffix' => '</div>',
  );
  
  $form['filters']['email'] = array(
    '#type' => 'textfield',
    '#title' => $filters['email']['title'],
    '#default_value' => $session['email'],
  );
  $form['filters']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#prefix' => '<span class="spacer" />',
  );
  // Add Reset button if filter is in use
  if ($session != _sndb_subscriptions_subscription_filter_default()) {
    $form['filters']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
  }
  $form['#submit'][] = 'sndb_subscriptions_subscription_filter_form_submit';

  return $form;
}

/**
 * Helper function: returns subscription filter default settings
 */
function _sndb_subscriptions_subscription_filter_default() {
  return array('newsletter' => 'all', 'email' => '');
}

function sndb_subscriptions_subscription_filter_form_submit($form, &$form_state) {
  switch ($form_state['values']['op']) {
    case t('Filter'):
      $_SESSION['sndb_subscriptions_subscriptions_filter'] = array(
        'newsletter' => $form_state['values']['newsletter'],
        'email' => $form_state['values']['email'],
      );
      break;
    case t('Reset'):
      $_SESSION['sndb_subscriptions_subscriptions_filter'] = _sndb_subscriptions_subscription_filter_default();
      break;
  }
}

/**
 * Build query for subscription filters based on session var content.
 *
 * @return array of SQL query parts
 *   array('where' => $where, 'join' => $join, 'args' => $args)
 */
function sndb_subscriptions_build_subscription_filter_query() {
  // Variables $args and $join are currently not used but left in for future extensions
  $where = $args = array();
  $join = '';

  // Build query
  if (isset($_SESSION['sndb_subscriptions_subscriptions_filter'])) {
    foreach ($_SESSION['sndb_subscriptions_subscriptions_filter'] as $key => $value) {
    switch ($key) {
        /*case 'newsletter':
          if ($value != 'all') {
            list($key, $value) = explode('-', $value, 2);
            $where[] = 's.'. $key .' = '. $value;
            $where[] = 's.status = 1';
          }
          break;*/
        case 'email':
          if (!empty($value)) {
            $where[] = "ss.mail LIKE '%%". db_escape_string($value) ."%%'";
          }
          break;
      }
      $args[] = $value;
    }
  }

  // All conditions are combined with AND
  $where = empty($where) ? '' : ' WHERE '. implode(' AND ', $where);

  return array('where' => $where, 'join' => $join, 'args' => $args);
}


/**
 * Theme subscription administration overview.
 */
function theme_sndb_subscriptions_subscription_list($form) {
  // Subscription table header
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Email'), 'field' => 'ss.mail', 'sort' => 'asc'),
    array('data' => t('Username'), 'field' => 'u.name'),
    array('data' => t('Status'), 'field' => 'ss.activated'),
  );
  if (variable_get('language_count', 1) > 1) {
    $header[] = array('data' => t('Language'), 'field' => 'ss.language');
  }
 // $header[] = t('Operations');
  $colcount = count($header);

  // Subscription table
  $output = drupal_render($form['options']);
  if (isset($form['mail']) && is_array($form['mail'])) {
    foreach (element_children($form['mail']) as $key) {
      $row = array();
      $row[] = drupal_render($form['snids'][$key]);
      $row[] = drupal_render($form['mail'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['status'][$key]);
      if (variable_get('language_count', 1) > 1) {
        $row[] = drupal_render($form['language'][$key]);
      }
      $row[] = drupal_render($form['operations'][$key]);
      $rows[] = $row;
    }
  }
  else  {
    $rows[] = array(array('data' => t('No subscriptions available.'), 'colspan' => $colcount));
  }

  // Render table header, pager and form
  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }
  $output .= drupal_render($form);

  return $output;
}

/**
 * Process variables to format the simplenews status indication.
 *
 * $variables contains:
 * - $source
 * - $status
 *
 * @see sndb-subscriptions-status.tpl.php
 * @see theme_sndb_subscriptions_status()
 */
function template_preprocess_sndb_subscriptions_status(&$variables) {
  switch ($variables['source']) {
    case 'published':
      $images = array(0 => 'sn-saved.png', 1 => 'sn-sent.png');
      $title = array(0 => t('Not published'), 1 => t('Published'));
      break;
    case 'activated':
      $images = array(0 => 'sn-saved.png', 1 => 'sn-sent.png');
      $title = array(0 => t('Inactive: E-mail is marked as deactivated'), 1 => t('Active: This e-mail address is active'));
      break;
    case 'sent':
      $images = array(
        SIMPLENEWS_STATUS_SEND_PENDING => 'sn-cron.png',
        SIMPLENEWS_STATUS_SEND_READY => 'sn-sent.png'
      );
      $title = array(
        SIMPLENEWS_STATUS_SEND_NOT => '-',
        SIMPLENEWS_STATUS_SEND_PENDING => $variables['already_sent'] . ' / ' . $variables['sent_subscriber_count'],
        SIMPLENEWS_STATUS_SEND_READY => $variables['sent_subscriber_count'],
      );
      $variables['trailer'] = $title[$variables['status']];
      break;
  }

  // Build the variables
  if (isset($images) && $images[$variables['status']]) {
    $variables['image'] = base_path() . drupal_get_path('module', 'sndb_subscriptions') .'/'. $images[$variables['status']];
  }
  $variables['alt'] = $variables['title'] = $title[$variables['status']];
}

function sndb_subscriptions_get_priority() {
  return array(
    SIMPLENEWS_PRIORITY_NONE => t('none'),
    SIMPLENEWS_PRIORITY_HIGHEST => t('highest'),
    SIMPLENEWS_PRIORITY_HIGH => t('high'),
    SIMPLENEWS_PRIORITY_NORMAL => t('normal'),
    SIMPLENEWS_PRIORITY_LOW => t('low'),
    SIMPLENEWS_PRIORITY_LOWEST => t('lowest'),
  );
}
