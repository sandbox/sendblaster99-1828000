<?php


/**
 * FAPI ACCOUNT subscription form.
 *
 * Finally _account_ cases inject into hook_user and won't work on its own.
 * Note that our basis is:
 * drupal_get_form('user_profile_form', ...);
 * and NOT:
 * drupal_get_form('sndb_subscriptions_subscriptions_account', ...);
 *
 * see also user/user.module and user/user.pages.inc
 *
 * @see sndb_subscriptions_subscriptions_account_form_validate()
 * @see sndb_subscriptions_subscriptions_account_form_submit()
 */
function sndb_subscriptions_subscriptions_account_form(&$form_state, $account) {
  $subscription = sndb_subscriptions_get_subscription($account);

  $form = array();
  $options = array();
  $default_value = array();

  // Get newsletters for subscription form checkboxes.
  // Newsletters with opt-in/out method 'hidden' will not be listed.
  foreach (sndb_subscriptions_get_newsletters(variable_get('sndb_subscriptions_vid', '')) as $newsletter) {
    $options[$newsletter->tid] = check_plain($newsletter->name);
    $default_value[$newsletter->tid] = FALSE;
  }

  $form['subscriptions'] = array(
    '#type' => 'fieldset',
    '#description' => t('Select your newsletter subscriptions.'),
  );
  $form['subscriptions']['newsletters'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => array_merge($default_value, (array)$subscription->tids),
  );

  $form['subscriptions']['#title'] =  t('Current newsletter subscriptions');

  // if we don't override #validate, see user_profile_form_validate

  // adding an own #submit leads to the situation where drupal omits execution of user_profile_form_submit completely
  $form['#submit'][] = 'sndb_subscriptions_subscriptions_account_form_submit';

  return $form;
}

/**
 * FAPI ACCOUNT subscription form_submit.
 */
function sndb_subscriptions_subscriptions_account_form_submit($form, &$form_state) {
  global $user;
  // Get current subscriptions if any. Defined in user_profile_form
  $account = $form_state['values']['_account'];

  // We first subscribe, then unsubscribe. This prevents deletion of subscriptions
  // when unsubscribed from the
  arsort($form_state['values']['newsletters'], SORT_NUMERIC);
  foreach ($form_state['values']['newsletters'] as $tid => $checked) {
    if ($checked) {
      sndb_subscriptions_subscribe_user($account->mail, $tid, FALSE, 'website');
    }
    else {
      sndb_subscriptions_unsubscribe_user($account->mail, $tid, FALSE, 'website');
    }
  }
  if ($user->uid==$account->uid) {
    drupal_set_message(t('Your newsletter subscriptions have been updated.'));
  }
  else {
    drupal_set_message(t('The newsletter subscriptions for user %account have been updated.', array('%account' => $account->name)));
  }
}

/**
 * FAPI BLOCK subscription form.
 *
 * @param $tid term id of selected newsletter.
 *
 * @see sndb_subscriptions_block_form_validate()
 * @see sndb_subscriptions_block_form_submit()
 */
function sndb_subscriptions_block_form(&$form_state) {
  global $user;
  $form = array();

  /*
  $form['subscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
    '#attributes' => array('class' => 'sndb-subscriptions-subscribe'),
    '#weight' => 20,
  );
  
  $form['unsubscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Unsubscribe'),
    '#attributes' => array('class' => 'sndb-subscriptions-unsubscribe'),
    '#weight' => 21,
  );
  */
  
  $form['subscription_state'] = array(
    '#type' => 'radios',
    '#title' => t('Action'),
    '#default_value' => 'subscribe',
    '#options' => array('subscribe' => t('Subscribe'), 'unsubscribe' => t('Unsubscribe')),
    '#required' => TRUE,
    '#weight' => 98,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    //'#attributes' => array('class' => 'sndb-subscriptions-subscribe'),
    '#weight' => 99,
  );
  
  if ($user->uid) {
    if (sndb_subscriptions_user_is_subscribed($user->mail)) {
      unset($form['subscribe']); // if the user is subscribed, allow only unsubscription
    }
    else {
      unset($form['unsubscribe']); // if the user is not subscribed, allow only subscription
    }
    $form['mail'] = array('#type' => 'value', '#value' => $user->mail);
  }
  else { // anonymous user
    
    $form['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#size' => 20,
      '#maxlength' => 128,
      '#required' => TRUE,
    );
    
   for($i = 1; $i <= SNDB_SUBSCRIPTION_EXTRA_FIELDS_MAX_CNT; $i++) {
     $curr_title = variable_get("sndb_subscriptions_extra_field_{$i}_title", '');
     if(!empty($curr_title)) {
        $form["extra_field_{$i}"] = array(
          '#type' => 'textfield',
          '#title' => $curr_title,
          '#size' => 20,
          '#maxlength' => 128,
        );
      }
    }
    
    //$form['action'] = array('#type' => 'value', '#value' => 'subscribe');
    $form['#attributes'] = array('class' => 'sndb-subscriptions-subscribe');
  }

  $form['#validate'][] = 'sndb_subscriptions_block_form_validate';
  $form['#submit'][] = 'sndb_subscriptions_block_form_submit';
  
  return $form;
}

/*
 * FAPI BLOCK subscription form_validate.
 */
function sndb_subscriptions_block_form_validate($form, &$form_state) {
  if (!sndb_subscriptions_valid_email_address($form_state['values']['mail'])) {
    form_set_error('mail', t("The email address you supplied is not valid."));
  }
}

/*
 * FAPI BLOCK subscription form_submit.
 */
function sndb_subscriptions_block_form_submit($form, &$form_state) {
  
  global $user;
  $account = _sndb_subscriptions_user_load($form_state['values']['mail']);
  
  // If email belongs to the current registered user, don't send confirmation.
  // Other addresses receive a confirmation if double opt-in is selected.
  if ($account->uid && $account->uid == $user->uid) {
    $confirm = FALSE;
  }
  else {
    $confirm = TRUE;
  }

  // switch depends on class (which doesn't change with language, but title changes)
  switch ($form_state['values']['subscription_state']) {
    case 'subscribe':
      
      $mail = trim($form_state['values']['mail']);
      $mail = check_plain($mail);
      
      $extra_fields_data = array();
      for($i = 1; $i <= SNDB_SUBSCRIPTION_EXTRA_FIELDS_MAX_CNT; $i++) {
        $curr_title = variable_get("sndb_subscriptions_extra_field_{$i}_title", '');
        if(!empty($curr_title)) {
          $curr_extra_val = trim($form_state['values']["extra_field_{$i}"]);
          if(!empty($curr_extra_val)) {
            $curr_extra_val = check_plain($curr_extra_val);
            $extra_fields_data["extra_field_{$i}"] = $curr_extra_val;
          }
        }
      }
      
      sndb_subscriptions_subscribe_user($mail, $tid, $confirm, 'website', NULL, $extra_fields_data);
      if ($confirm) {
        drupal_set_message(t('You will receive a confirmation email shortly containing further instructions on how to complete your subscription.'));
      }
      else {
        drupal_set_message(t('You have been subscribed.'));
      }
      break;
      
    case 'unsubscribe':
      $mail = trim($form_state['values']['mail']);
      $mail = check_plain($mail);
      
      sndb_subscriptions_unsubscribe_user($mail, $tid, $confirm, 'website');
      if ($confirm) {
        drupal_set_message(t('You will receive a confirmation email shortly containing further instructions on how to cancel your subscription.'));
      }
      else {
        drupal_set_message(t('You have been unsubscribed.'));
      }
      break;
  }
}


/**
 * Menu callback: confirm the user's (un)subscription request
 *
 * This function is called by clicking the confirm link in the confirmation
 * email or the unsubscribe link in the footer of the newsletter. It handles
 * both subscription addition and subscription removal.
 *
 * Calling URLs are:
 * newsletter/confirm/add
 * newsletter/confirm/add/$HASH
 * newsletter/confirm/remove
 * newsletter/confirm/remove/$HASH
 *
 * @see sndb_subscriptions_confirm_add_form()
 * @see sndb_subscriptions_confirm_removal_form()
 */
function sndb_subscriptions_confirm_subscription() {
  $arguments = func_get_args();
  // add or remove
  $op1 = array_shift($arguments);
  // private key hash
  $op2 = array_shift($arguments);

  $md5 = drupal_substr($op2, 0, 10);
  $snid = drupal_substr($op2, 10);

  $result = db_query('
    SELECT snid, mail
    FROM {sndb_subscriptions}
    WHERE snid = %d',
    $snid);
  if (!($subs = db_fetch_object($result))) {
    drupal_not_found();
    return;
  }

  // Prevent search engines from indexing this page.
  drupal_set_html_head('<meta name="robots" content="noindex" />');

  if ($md5 == drupal_substr(md5($subs->mail . sndb_subscriptions_private_key()), 0, 10)) {
    //$newsletter = taxonomy_get_term($tid);
    // The confirmation page called with two arguments will display a confirmation question.
    // When called with three of more arguments the user will be directed to the
    // (un)subscribe confirmation page. The additional arguments will be passed on
    // to the confirmation page.
    //if (empty($arguments)) {
      if ($op1 == 'remove') {
        return drupal_get_form('sndb_subscriptions_confirm_removal_form', $subs->mail);
      }
      elseif ($op1 == 'add') {
        return drupal_get_form('sndb_subscriptions_confirm_add_form', $subs->mail);
      }
    /*}
    else {
      if ($op1 == 'remove') {
        sndb_subscriptions_unsubscribe_user($subs->mail, $tid, FALSE, 'website');
        if ($path = variable_get('sndb_subscriptions_confirm_unsubscribe_page', '')) {
          $path = $path .'/'. implode('/', $arguments);
          drupal_goto($path);
        }
        drupal_set_message(t('%user was unsubscribed from the %newsletter mailing list.', array('%user' => $subs->mail, '%newsletter' => $newsletter->name)));
        drupal_goto(variable_get('site_frontpage', 'node'));
      }
      elseif ($op1 == 'add') {
        sndb_subscriptions_subscribe_user($subs->mail, $tid, FALSE, 'website');
        if ($path = variable_get('sndb_subscriptions_confirm_subscribe_page', '')) {
          $path = $path .'/'. implode('/', $arguments);
          drupal_goto($path);
        }
        drupal_set_message(t('%user was added to the %newsletter mailing list.', array('%user' => $subs->mail, '%newsletter' => $newsletter->name)));
        drupal_goto(variable_get('site_frontpage', 'node'));
      }
    }*/
  }

  // If md5 didn't match, do a not found.
  drupal_not_found();
  return;
}

/**
 * FAPI CONFIRM ADD form.
 *
 * @see sndb_subscriptions_confirm_add_form_submit()
 */
function sndb_subscriptions_confirm_add_form(&$form_state, $mail) {
  $form = array();
  $form['question'] = array('#value' => '<p>'. t('Are you sure you want to add %email address to the mailing list?',
      array('%email' => sndb_subscriptions_mask_mail($mail))) ."<p>\n");
  $form['mail'] = array('#type' => 'value', '#value' => $mail);
  $form['#redirect'] = '';
  
  return confirm_form($form,
    t('Confirm subscription'),
    '',
    t('If you subscribe, you will receive newsletters.'),
    t('Subscribe'),
    t('Cancel')
  );
}

/**
 * FAPI CONFIRM ADD form_submit.
 */
function sndb_subscriptions_confirm_add_form_submit($form, &$form_state) {
  sndb_subscriptions_subscribe_user($form_state['values']['mail'], 0, FALSE, 'website');
  drupal_set_message(t('%email was added to the mailing list.', array('%email' => $form_state['values']['mail'])));

}

/**
 * Mask a mail address.
 *
 * For example, name@example.org will be masked as n*****@e*****.org.
 *
 * @param $mail
 *   A valid mail address to mask.
 *
 * @return
 *   The masked mail address.
 */
function sndb_subscriptions_mask_mail($mail) {
  if (preg_match('/^(.).*@(.).*(\..+)$/', $mail)) {
    return preg_replace('/^(.).*@(.).*(\..+)$/', '$1*****@$2*****$3', $mail);
  }
  else {
    // Missing top-level domain.
    return preg_replace('/^(.).*@(.).*$/', '$1*****@$2*****', $mail);
  }
}

/**
 * FAPI CONFIRM REMOVAL form.
 *
 * @see sndb_subscriptions_confirm_removal_form_submit()
 */
function sndb_subscriptions_confirm_removal_form(&$form_state, $mail) {
  $form = array();
  $form['question'] = array('#value' => '<p>'. t('Are you sure you want to remove %email from the newsletter?', array('%email' => sndb_subscriptions_mask_mail($mail))) ."<p>\n");
  $form['mail'] = array('#type' => 'value', '#value' => $mail);
  $form['#redirect'] = '';
  
  return confirm_form($form,
    t('Confirm subscription deletion'),
    '',
    t('If you unsubscribe, you will no longer receive newsletters.'),
    t('Unsubscribe'),
    t('Cancel')
  );
}

/**
 * FAPI CONFIRM REMOVAL form_submit.
 */
function sndb_subscriptions_confirm_removal_form_submit($form, &$form_state) {
  sndb_subscriptions_unsubscribe_user($form_state['values']['mail'], 0, FALSE, 'website');
  drupal_set_message(t('%email was unsubscribed from the newsletter.', array('%email' => $form_state['values']['mail'])));
}


/**
 * Menu callback: exports the subscription data into CSV
 *
 * Calling URL is:
 * sndb_subscriptions/export/$HASH
 *
 */
function sndb_subscriptions_export_csv() {
  if(variable_get('sndb_subscriptions_export_link_enabled', FALSE)) {
    $arguments = func_get_args();
    
    // Export type
    $subscription_filter = array_shift($arguments);
    if($subscription_filter == 'active') {
      $subscription_filter = SNDB_SUBSCRIPTION_STATUS_SUBSCRIBED;
    }
    else {
      $subscription_filter = SNDB_SUBSCRIPTION_STATUS_UNSUBSCRIBED;
    }
    
    // private key hash
    $link_hash = array_shift($arguments);
    $saved_hash_token = variable_get('sndb_subscriptions_export_link_token', FALSE);
    if($link_hash == $saved_hash_token) {
      echo sndb_subscriptions_get_subscriptions_as_csv_data($subscription_filter);
    }
    else { // wrong secret token
      drupal_not_found();
    }
  }
  else { // export is not allowed
    drupal_not_found();
  }
}


function sndb_subscriptions_get_subscriptions_as_csv_data($subscription_filter = SNDB_SUBSCRIPTION_STATUS_SUBSCRIBED) {
  $data = array();
  
  $query = '
    SELECT DISTINCT s.mail, s.activated, s.timestamp, s.language, s.extra_fields
    FROM {sndb_subscriptions} s
    WHERE activated = %d';
  
  $extra_fields_defaults = array(); // create default array
  for($i = 1; $i <= SNDB_SUBSCRIPTION_EXTRA_FIELDS_MAX_CNT; $i++) {
    $extra_fields_defaults["extra_field_{$i}"] = '';
  }
  
  $result = db_query($query, $subscription_filter);
  while ($subscription = db_fetch_array($result)) {
    
    $curr_extra_fields = unserialize($subscription['extra_fields']) + $extra_fields_defaults;
    unset($subscription['extra_fields']);
    $data[] = array_merge($subscription, $curr_extra_fields);
    
  }
  
  if(!empty($data)) {
    return sndb_subscriptions_array_to_scv($data);
  }
  else {
    return FALSE;
  }
}


/**
 * Generatting CSV formatted string from an array.
 * By Sergey Gurevich.
 * @see http://www.codehive.net/PHP-Array-to-CSV-1.html
 */
function sndb_subscriptions_array_to_scv($array, $header_row = true, $col_sep = ";", $row_sep = "\n", $qut = '"')
{
  if (!is_array($array) or !is_array($array[0])) {
    return false;
  }

  //Header row.
  if ($header_row) {
    foreach ($array[0] as $key => $val) {
      //Escaping quotes.
      $key = str_replace($qut, "$qut$qut", $key);
      $output .= "$col_sep$qut$key$qut";
    }
    $output = substr($output, 1)."\n";
  }
  
  //Data rows.
  foreach ($array as $key => $val) {
    $tmp = '';
    foreach ($val as $cell_key => $cell_val) {
      //Escaping quotes.
      $cell_val = str_replace($qut, "$qut$qut", $cell_val);
      $tmp .= "$col_sep$qut$cell_val$qut";
    }
    $output .= substr($tmp, 1).$row_sep;
  }

  return $output;
}

